interface PointLike {
    x: number;
    y: number;
}

interface Named {
    name: string;
}


export class erasedStructuralTypes {

    obj2: any;
    
    logPoint(point: PointLike){
        console.log("x = " + point.x + ", y = " + point.y);
    }
    
    logName(x: Named){
        console.log("Hello " + x.name);
    }
    constructor(){

        this.obj2 = {
            x: 0,
            y: 0,
            name: "Origin",
        };
    }
}

