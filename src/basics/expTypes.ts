// how to force explicit types
export class expTypes {
    Demo(){
        let myString: string = "This is a string";
        let myNum: number = 22;
        let myBool: boolean = true;

        let myUndefined: string;
        myUndefined = "hello";
        // myUndefined = 0;
        // myUndefined = true;
     
        /*
            this is not possible
            let myNull: boolean = null;
            myNull = "hello";
            myNull = 0;
         */

        let myNull: boolean;
        myNull = false;

        let myNull2: boolean | null = null
        console.trace(myString, myNum, myBool, myUndefined, myNull, myNull2);
    }

    printString(s: string){
        console.log(s.toUpperCase());
    };
    constructor(){}
}



