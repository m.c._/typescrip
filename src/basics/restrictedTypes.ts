// Define a TypeScript class called `restrictedTypes`
export class restrictedTypes {

    // Constructor for the class
    constructor(){}

    // Demonstrate the usage of restricted types and annotations
    Demo() {
        // Define a constant `DARK_MODE` with a boolean value `false`
        let DARK_MODE: false = false; // Can only be assigned `false`

        // Simulate a coin flip
        let coin = Math.floor(Math.random() * 2);
        // Determine the result based on the coin value
        let result = coin === 0 ? "Heads" : "Tails"; // Using ternary operator
        console.log(result); // Output the result
        console.log(coin); // Output the coin value
        console.log(coin === 0 ? "Heads" : "Tails"); // Another ternary operator example

        // Define a variable `errorCode` with specific string literal types
        let errorCode: 'ERROR_404' | 'ERROR_500' | 'ERROR_503';

        // Assign the value 'ERROR_500' to `errorCode`
        errorCode = 'ERROR_500'; // Can only be one of the specified strings
    }
}
