export class theBasics {

    flipCoin() {
        // Meant to be Math.random()
        //  return Math.random < 0.5;
        //Operator '<' cannot be applied to types '() => number' and 'number'.
    }
    message = '';
    announcement = 'This Is A Test';

    constructor () {
        const user = {
            name: "Daniel",
            age: 26,
        };
        const value = Math.random() < 0.5 ? "a" : "b";
          
        // static type-checking
        // calling 'message'
        //message(); //-> TypeError: message is not a function
        // It’d be great if we could avoid mistakes like this.
        // When we run our code, the way that our JavaScript runtime chooses what to do is by figuring out the type of the value - what sorts of behaviors and capabilities it has. 
        // That’s part of what that TypeError is alluding to - it’s saying that the string "Hello World!" cannot be called as a function.
        
        //Non-exception Failures
        
        
        //user.location; //returns undefined in JavaScript
        // TypeScript gives: Property 'location' does not exist on type '{ name: string; age: number; }'.ts(2339)    
        
        if (value !== "a") {
            // ...
        } /*else if (value === "b") {
            //This condition will always return 'false' since the types '"a"' and '"b"' have no overlap.
            // Oops, unreachable
        }*/
    }
}