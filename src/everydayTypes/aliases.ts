/**
 * Type Aliases
 * We’ve been using object types and union types by writing them directly in type annotations.
 * This is convenient, but it’s common to want to use the same type more than once and
 * refer to it by a single name.
 * A type alias is exactly that - a name for any type.
 * The syntax for a type alias is:
 */
type Point = {
    x: number;
    y: number;
}

/**
 *  You can actually use a type alias to give a name to any type at all, not just an object type.
 * For example, a type alias can name a union type:
 */
type ID = number | string;



export class aliases{
    point: Point = { x: 100, y: 100 };
    
    // Exactly the same as the earlier example
    printCoord(pt: Point) {
        console.log("The coordinate's x value is " + pt.x);
        console.log("The coordinate's y value is " + pt.y);
    }
    
    printId(id: ID) {
        if(typeof(id) === "string"){
            console.log("Your ID is: " + id.toUpperCase());
        } else {
            console.log("Your ID is: " + id);
        }
    }

    constructor(){
        console.log("The coordinate's x value is " + this.point.x);
        console.log("The coordinate's y value is " + this.point.y);
    }
}