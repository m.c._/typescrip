let objTest: any = { x: 0};
// None of the following will throw an error
// using 'any' disables all type checking
// it is assumed you know your code best
objTest.name();
objTest();
objTest.bar = 100;
objTest ='Hello';
const n: number = objTest;