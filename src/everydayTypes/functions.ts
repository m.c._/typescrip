export class functions{

    // Parameter with type annotation
    /**
     * greets witch the given name
     * @param name 
     */
    greet(name: string) {
        console.log("Hello, " + name.toLowerCase() + "!!");
    }
    
    //return type annotation
    /**
     * returns a number
     * @returns 
     */
    getFavoriteNumber(): number {
        return 26;
    }

    /**
     *This is the constructor of this class 
     */
    constructor(){

    }
}    
