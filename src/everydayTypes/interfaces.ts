/**
 * Interface defined called Point that has two properties - x and y, both of type number.
 * You then have a function called printCoord that accepts a parameter of type Point. 
 * Inside the function, it uses console.log to output the x and y values of the passed in Point object.
 * Finally, you call the printCoord function, passing in an object literal with x and y properties set to 100. 
 * This will cause it to log "The coordinate's x value is 100" and "The coordinate's y value is 100" to the console.
 * Interfaces in TypeScript are useful for defining contracts that objects need to adhere to.
 * Here, by defining the Point interface, any object passed to functions expecting a Point must have x and y properties of the correct type in order to pass validation. This helps catch errors and get early feedback in your editor.
 */

export interface Point{
    x: number;
    y: number;
}
