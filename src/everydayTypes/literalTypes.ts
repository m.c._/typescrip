/**
 * The code sample is defining a TypeScript class called literalTypes that demonstrates how TypeScript handles string literals.
 * It first declares two string variables - changingString initialized to "Hello World",
 * and constantString also initialized to "Hello World".
 * The key difference is that changingString is declared with let, meaning its value can be changed. 
 * constantString is declared with const, meaning its value cannot be changed.
 * The literalTypes class constructor is then defined. Inside the constructor:
 * It changes the value of changingString to "Olá Mundo" using assignment.
 * This is allowed since changingString was declared with let.
 * It logs both changingString and constantString to the console.
 * We will see changingString now holds the new value, but constantString still holds 
 * its original value since it cannot be reassigned.
 * So in summary, this code shows how TypeScript treats string literals 
 * differently based on whether the variable is declared with let or const. 
 * let variables can have their literal value changed, while const variables treat the literal as a fixed value 
 * that cannot be altered. This helps catch errors and enforce immutability where intended.
 */
interface Options {
    width: number;
}

let changingString = "Hello World";
const constantString = "Hello World";
const obj = { counter: 0 };
const req = { url: "https://example.com", method: "GET" };

export class literalTypes {

    /**
     * the constructor is used to change the value of changingString 
     * to "Olá Mundo" and log both changingString and constantString to the console.  
     */
    constructor() {
        changingString = "Olá Mundo";
        console.log(changingString);
        console.log(constantString);
    }

    /**
     * prints a text with alignment
     * @param s
     * @param alignment 
     */
    printText(s: string, alignment: "left" | "right" | "center") {
        // format the text in console from alignment
        switch (alignment) {
            case "left":
                return s.padEnd(60);

            case "right":
                return s.padStart(60);

            case "center":
                return s.padStart((60 - s.length) / 2 + s.length);

            default:
                throw new Error("Invalid alignment value");
        }
        console.log(s);
    }

    /**
     * The compare function defined in the code sample takes in two string parameters, 
     * a and b, and returns -1, 0 or 1 depending on how the strings compare. [1]
     * It uses the ternary operator (? :) for a concise way to return the comparison value: [2]
     * It first checks if the two strings are equal using a === b. If they are equal, it returns 0.
     * If they are not equal, it checks if a is greater than b using a > b. If a is greater, it returns 1.
     * If a is neither equal nor greater than b, then a must be less than b, so it returns -1.
     * So in simple terms, it compares the two strings and returns a value indicating whether the first string comes before, after, or is equal to the second string alphabetically. This allows you to programmatically determine the sorting order of strings in a simplified way.
     * @param a 
     * @param b 
     * @returns 
     */
    compare(a: string, b: string): -1 | 0 | 1 {
        return a === b ? 0 : a > b ? 1 : -1; // -1, 0, 1
    }

    configure(x: Options | "auto") {
        if (typeof x === "string") {
            console.log(x);
            return;
        }
        console.log(x.width);
    }

    /**
     * literal inference
     */
    checkObj(){
        console.log(obj.counter);
    }

    handleRequest(url: string, method: "GET" | "POST") {};
}