/**
 * With strictNullChecks on, when a value is null or undefined, 
 * you will need to test for those values before using methods or properties on that value. 
 * Just like checking for undefined before using an optional property, 
 * we can use narrowing to check for values that might be null:
 */

export class nullundef{
    doSomthing(x: string | null){
        if(x === null){
            // do nothing
        }
        else{
            console.log("Hello, " + x.toUpperCase());
        }
    }

    /**
     * Non-null Assertion Operator (Postfix ! )
     * Careful with strings they the .toXXXX methods run with an error
     * additional checks needed like:
     * console.log((x ?? '').toLowerCase());
     * So only use '!' when the value is definitely is not null !
     */
    doSomthing2(x?: string | null){
        console.log(x!);
    }
}