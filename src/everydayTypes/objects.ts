export class objects{

    /**
     * Prints the coordinates
     * @param pt 
     */
    printCoord(pt: { x: number; y: number }) {
        console.log("The coordinate's x value is " + pt.x);
        console.log("The coordinate's y value is " + pt.y);
      };

    /**
     * prints the Name
     * @param obj 
     */
    printName(obj: { first: string; last?: string }) {
        console.log(obj.first?.toUpperCase());
        // ...
        // Error - might crash if 'obj.last' wasn't provided!
        // console.log(obj.last.toUpperCase());
        // Object is possibly 'undefined'.
        // Object is possibly 'undefined'.
        if (obj.last !== undefined) {
            // OK
            console.log(obj.last.toUpperCase());
        }
   
        // A safe alternative using modern JavaScript syntax:
        console.log(obj.last?.toUpperCase());
      }

      /**
       * This is the constructor of objects.ts
       */
      constructor(){
          this.printCoord({ x: 3, y: 7 });
          // Both OK
          this.printName({ first: "Bob" });
          this.printName({ first: "Alice", last: "Alisson" });

    };
}