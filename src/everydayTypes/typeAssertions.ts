let i: number;
let j: number;

export class typeAssertions{
    constructor(){
        i = 5;
        j = i as number;
        console.log(j);

        // use any to mak a transition
        let a = i+j as any as number;
        console.log(a);
    }
}