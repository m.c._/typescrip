/**
 * TypeScript’s type system allows you to build new types out of existing ones
 * using a large variety of operators. Now that we know how to write a few types,
 * it’s time to start combining them in interesting ways.
 */

export class UnionTypes{
    /**
     * prints the input id
     * @param id
     */
    printId(id: number | string) {
        console.log("Your ID is: " + id);
    }

    /**
     * print the input id but checks also for its type
     * @param id 
     */
    printIdWithTypeCheck(id: number | string) {
        if(typeof(id) === "string"){
            console.log("Your ID is: " + id.toUpperCase());
        } else {
            console.log("Your ID is: " + id);
        }
    }

    /**
     * prints Hello depending if array oder string was given
     * @param x
     */
    welcomePrint(x: string[] | string) {
        if (Array.isArray(x)) {
            // Here: 'x' is 'string[]'
            console.log("Hello, " + x.join(" and "));
          } else {
            // Here: 'x' is 'string'
            console.log("Welcome lone traveler " + x);
          }
    }

    /**
     * gets the first 3 values slice method is present in arrays and strings
     * @param x 
     * @returns
     * Return type is inferred as number[] | string
     */
    getFirstThree(x: number[] | string) {
        return x.slice(0, 3);
    }

    /**
     * the constructor of this class
     */
    constructor(){
        this.printId("mike");
        // OK
        this.printId(101);
        // OK
        this.printId("202");
        // Error
        /**
         * this.printId({ myID: 22342 });
         * Argument of type '{ myID: number; }' 
         * is not assignable to parameter of type 'string | number'.ts(2345)
        */
       
       /**
        * how to deal with methods of the different variable types?
        * you will have to check what type the var actually is!
       */
      this.printIdWithTypeCheck(22);
      this.printIdWithTypeCheck("s22");
      this.printIdWithTypeCheck("mike");
    }
}