import { Empty } from "./basics/consequencesOfStructuralTyping";
import { erasedStructuralTypes } from "./basics/erasedStructuralTypes";
import { Car, Golfer } from "./basics/identicalTypes";
import { theBasics } from "./basics/theBasics";
import { aliases } from "./everydayTypes/aliases";
import { arrays } from "./everydayTypes/arrays";
import { functions } from "./everydayTypes/functions";
import { objects } from "./everydayTypes/objects";
import { primitives } from "./everydayTypes/primitives";
import { UnionTypes } from "./everydayTypes/unionTypes";
import { impTypes } from "./basics/impTypes";
import { expTypes } from "./basics/expTypes";
import { restrictedTypes } from "./basics/restrictedTypes";
import { Point } from "./everydayTypes/interfaces";
import { typeAssertions } from "./everydayTypes/typeAssertions";
import { literalTypes } from "./everydayTypes/literalTypes";
import { nullundef } from "./everydayTypes/null-undef";
import { lessCommonPrim } from "./everydayTypes/less-common-prim";
import { narrowingGeneral } from "./narrowing/general";
import { typeofs } from "./narrowing/typeofs";

const interf: Point = {
    x: 10,
    y: 20,
}
console.log('hi')

function fn(arg: Empty) {
    // do something
}


function printCoord(pt: Point) {
    console.log("The coordinate's x value is " + pt.x);
    console.log("The coordinate's y value is " + pt.y);
}

// No error, but this isn't an 'Empty' ?
fn({ k: 10 });

// TypeScript determines if the call to fn here is valid by seeing if the provided argument is a valid Empty.
// It does so by examining the structure of { k: 10 } and class Empty { }.
// We can see that { k: 10 } has all of the properties that Empty does,
// because Empty has no properties. Therefore, this is a valid call!

const basics = new theBasics();
basics.flipCoin();
// accessing the property 'toLowerCase'
// on 'message' and than calling it
console.log(basics.message.toLowerCase());
// We probably meant to write this...
console.log(basics.announcement.toLocaleLowerCase());

const erasedST = new erasedStructuralTypes();
erasedST.logPoint(erasedST.obj2);
erasedST.logName(erasedST.obj2);

// TypeScript’s type system is structural, not nominal: We can use obj as a Pointlike because it has x and y properties that are both numbers. 
// The relationships between types are determined by the properties they contain, not whether they were declared with some particular relationship.
// TypeScript’s type system is also not reified: There’s nothing at runtime that will tell us that obj is Pointlike. In fact, the Pointlike type is not present in any form at runtime.
// Going back to the idea of types as sets, we can think of obj as being a member of both the Pointlike set of values and the Named set of values.

//No error?
let w: Car = new Golfer();

// Again, this isn’t an error because the structures of these classes are the same. 
// While this may seem like a potential source of confusion, in practice, 
// identical classes that shouldn’t be related are not common.

let func = new functions();
//this will be a runtime error if executed!
// greet(42);
// must be
func.greet('Mila');

console.log(func.getFavoriteNumber());

// anonymous functions
//no type annotation here, but TypeScript can spot the bug

const names = ["Allice", "Bob", "Eve"];

// contextual typing for function
names.forEach(function (s) {
    // console.log(s.toUppercase());
    /*
    Property 'toUppercase' does not exist on type 'string'. Did you mean 'toUpperCase'?
    */
    console.log(s.toUpperCase());
});

// contextual typing also applies to arrow functions
names.forEach((s) => {
    // console.log(s.toUppercase());
    /*
    Property 'toUppercase' does not exist on type 'string'. Did you mean 'toUpperCase'?
    */
    console.log(s.toUpperCase());
});

let arr = new arrays();
console.log(arr.oneTowThree);
console.log(arr.stringArray);

let prim = new primitives();

console.log(prim.text + "\n" + prim.intNumber + "\n" + prim.isTrue);

let object = new objects();
object.printCoord({ x: 5, y: 5 });
object.printName({ first: "Mike" });
object.printName({ first: "Mike", last: "test" });

const unionTypes = new UnionTypes();
let people: string[];
let numbs: number[];
people = ["Dave", "Monika", "Debbie", "Mark"];
numbs = [1, 4, 2, 7];

unionTypes.welcomePrint(people);
console.log(unionTypes.getFirstThree(numbs));

let ali = new aliases();
ali.printCoord(ali.point);
ali.printId("jsfnfk")

const impTyp = new impTypes();
impTyp.Demo();

const expTyp = new expTypes();
expTyp.Demo();
expTyp.printString("hello");

const restricted = new restrictedTypes();
restricted.Demo();
printCoord(interf);

let assert = new typeAssertions();
let literalType = new literalTypes();

let literal = new literalTypes();
literal.printText("DoDoDo, A Dadada", "left");
literal.printText("DoDoDo, A Dadada", "center");
literal.printText("DoDoDo, A Dadada", "right");
let equal = literal.compare("left", "right");
console.log(equal);
let equal2 = literal.compare("left", "left");
console.log(equal2);
literal.configure("auto");
literal.configure({ width: 100 });

let req = {url: "https://example.com", method: "GET"};
// literalType.handleRequest(req.url, req.method); this will not work
let rightReq = { url: "https://example.com", method: "GET" as "GET"};
literalType.handleRequest(rightReq.url, rightReq.method); // this will work
let rightReq2 = { url: "https://example.com", method: "GET" } as const;
literalType.handleRequest(rightReq2.url, rightReq2.method); // this will work also 
/**
 * In TypeScript, type assertions allow you to override the type inferred by the compiler for an expression. [1]
 * The code sample is using type assertions to assert that the "method" property on the object is of type "GET",
 * so that it matches the expected type for the handleRequest method.
 * Without the assertion, the inferred type would be "string" which does not match what handleRequest expects.
 * By asserting it as "GET", we are telling the compiler that even though it is a string value,
 * it should be treated as the literal type "GET" within that code flow.
 * This allows the code to compile correctly by ensuring the method parameter matches
 * what the function implementation expects, while still keeping the flexibility of a string value at runtime
 */
console.log('Done not empty!')
console.log('lol')

/**
 * JavaScript has two primitive values used to signal absent or uninitialized value: null and undefined.
 * TypeScript has two corresponding types by the same names. 
 * How these types behave depends on whether you have the strictNullChecks option on.
 * With strictNullChecks off, values that might be null or undefined can still be accessed normally, 
 * and the values null and undefined can be assigned to a property of any type. 
 * This is similar to how languages without null checks (e.g. C#, Java) behave. 
 * The lack of checking for these values tends to be a major source of bugs; 
 * we always recommend people turn strictNullChecks on if it’s practical to do so in their codebase.
 */

let nullDef =  new nullundef();
nullDef.doSomthing(null);
nullDef.doSomthing("Dude");
nullDef.doSomthing2(null);
nullDef.doSomthing2("Dude, with Postfix '!' ");

/**
 * less common primitives
 */
let prim2 = new lessCommonPrim();

/**
 * Narrowing
 */

let narrowingGen = new narrowingGeneral();
console.log(narrowingGen.padLeft(12, 'bold'));

let typeOf = new typeofs();
let strings: string[] = ['a', 'b', 'c'];
typeOf.printAll('abc');
typeOf.printAll(strings);