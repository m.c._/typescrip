/**
 * General stuff related to narrowing
 */

export class narrowingGeneral{
    padLeft(padding: number | string, input: string) {
    /**
     * this allone will cause an type error in ts 
     * return " ".repeat(padding) + input; 
     */        
        if (typeof padding === "number") {
            return " ".repeat(padding) + input;
        }
        return padding + input;
    }
}