/**
 * As we’ve seen, JavaScript supports a typeof operator which can give very 
 * basic information about the type of values we have at runtime. 
 * TypeScript expects this to return a certain set of strings:
 * "string"
 * "number"
 * "bigint"
 * "boolean"
 * "symbol"
 * "undefined"
 * "object"
 * "function"
 * In TypeScript, checking against the value returned by typeof is a type guard. 
 * Because TypeScript encodes how typeof operates on different values, it knows about some of its quirks in JavaScript. 
 * For example, notice that in the list above, typeof doesn’t return the string null. Check out the following example:
 */

export class typeofs{
    printAll(strs: string | string[] | null) {
        if (typeof strs === "object") {
          for (const s of strs!/**null handling!*/) {
            console.log(s);
          }
        } else if (typeof strs === "string") {
          console.log(strs);
        } else {
          // do nothing
        }
      }
}